package ru.bostonsd.study.springcloud.bankservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.bostonsd.study.springcloud.bankservice.model.User;

@FeignClient("user-service") //service name registered in service discovery
public interface UserClient {

    @GetMapping("/user/{userId}")
    public User getUser(@PathVariable("userId") int userId);

    @GetMapping("/test")
    public ResponseEntity test();
}
