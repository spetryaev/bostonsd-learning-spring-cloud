package ru.bostonsd.study.springcloud.bankservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bostonsd.study.springcloud.bankservice.client.UserClient;
import ru.bostonsd.study.springcloud.bankservice.model.User;

import javax.xml.ws.Response;

@Slf4j
@RestController
public class BankServiceController {

    @Autowired
    private UserClient userClient;

    @GetMapping("/user")
    public ResponseEntity getUser(){
        User user = userClient.getUser(1);
        /*if(clientResponse.getBody() instanceof User){
            User user = (User)clientResponse.getBody();
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        log.info(user.toString());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
