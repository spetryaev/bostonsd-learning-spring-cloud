package ru.bostonsd.study.springcloud.bankservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class SpringCloudBankServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudBankServiceApplication.class, args);
	}

}
