package ru.bostonsd.study.springcloud.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2 Configuration Class
 *
 * @date 08-Mar-2019
 * @author Sergey Petryaev spetryaev@bostonsd.ru
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {

    @Bean
    public Docket apiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())  //path to controller(s)!
                .build();
    }

    //TODO metadata
}
