package ru.bostonsd.study.springcloud.userservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.bostonsd.study.springcloud.userservice.model.User;

@Slf4j
@RestController
public class UserController {

    @GetMapping("/user/{userId}")
    public User getUser(@PathVariable("userId") int userId){
        User user = new User();
        user.setId(1);
        user.setUsername("adminuser");
        log.info("Sending data: " + user.toString());
        return user;
    }

    @GetMapping("/test")
    public ResponseEntity test(){
        User user = new User();
        user.setId(1);
        user.setUsername("adminuser");
        log.info("Sending data: " + user.toString());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
