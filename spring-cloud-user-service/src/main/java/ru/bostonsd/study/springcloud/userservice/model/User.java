package ru.bostonsd.study.springcloud.userservice.model;

import lombok.Data;

@Data
public class User {

    private int id;
    private String username;

}
