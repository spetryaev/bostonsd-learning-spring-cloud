package ru.bostonsd.study.springcloud.groupservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringCloudGroupServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudGroupServiceApplication.class, args);
	}

}
